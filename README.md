## INTRODUCTION

The Diboo signature pad module is an addon for the diboo core module
that configures a signature pad widget to draw a picture for a chain.

First module for Diboo that enables drawing.

## REQUIREMENTS

[Signature pad](https://www.drupal.org/project/signature_pad)

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- None, image chain links will use the signature pad widget.

## ROADMAP

- Make this module optional when there is more modules to draw pictures.

## MAINTAINERS

Current maintainers for Drupal 10:

- Rodrigo Aguilera (rodrigoaguilera) - https://www.drupal.org/u/rodrigoaguilera

